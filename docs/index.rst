.. Traffic Replay Library documentation master file, created by
   sphinx-quickstart on Wed Sep 11 14:04:28 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Traffic Replay Library's documentation!
==================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Traffic Replay Library (TRLib) is a collection of utilities and a replay format parser for use by Traffic Replay and Microserver. These tools are used in conjunction with AuTest for testing Apache Traffic Server. 

The replay format is located in schema/replay_format.json. 


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


